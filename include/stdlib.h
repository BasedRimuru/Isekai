/*
This file is part of Isekai.

Isekai is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 2 of the License,
or (at your option) any later version.

Isekai is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Isekai. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _STDLIB_H
#define _STDLIB_H

#include <stddef.h>

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1
void abort(void);
int atexit(void (*func)(void));
void _Exit(int status);
void exit(int status);

int system(const char *command);
char *getenv(const char *name);

#define RAND_MAX 0xffffffff
int rand(void);
void srand(unsigned int seed);

int abs(int x);
long labs(long x);
long long llabs(long long x);

typedef struct { int quot; int rem; } div_t;
typedef struct { long quot; long rem; } ldiv_t;
typedef struct { long long quot; long long rem; } lldiv_t;
div_t div(int dividend, int divisor);
ldiv_t ldiv(long dividend, long divisor);
lldiv_t lldiv(long long dividend, long long divisor);

int atoi(const char *str);
long atol(const char *str);
long long atoll(const char *str);
double atof(const char *str);

#endif
