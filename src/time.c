/*
This file is part of Isekai.

Isekai is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 2 of the License,
or (at your option) any later version.

Isekai is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Isekai. If not, see <https://www.gnu.org/licenses/>.
*/

#include <time.h>
#include <limits.h>

#include "isekai/syscall.h"

/* Time manipulation */

#if defined(__linux__)

#define CLOCK_REALTIME 0
#define CLOCK_PROCESS_CPUTIME_ID 2
struct timespec
{
    time_t tv_sec;
    long tv_nsec;
};
#include <sys/syscall.h>

#else
#error Unimplemented target
#endif

extern clock_t
clock(void)
{
    clock_t ret = -1;

    #if defined(__linux__)
    struct timespec result = {0};
    if (__syscall2(SYS_clock_gettime, CLOCK_PROCESS_CPUTIME_ID,
                   (__syscall_t)&result) == 0)
    {
        if (result.tv_sec <= (time_t)LONG_MAX/CLOCKS_PER_SEC &&
            result.tv_nsec <= (time_t)(LONG_MAX - (CLOCKS_PER_SEC *
                                                   result.tv_sec)))
        {
            ret = result.tv_sec * CLOCKS_PER_SEC;
            ret += result.tv_nsec / (CLOCKS_PER_SEC / 1000);
        }
    }
    #else
    #error Unimplemented target
    #endif

    return ret;
}

extern double
difftime(time_t time, time_t time2)
{
    return (double)(time - time2);
}

extern time_t
mktime(struct tm *timeptr)
{
    return 0;
}

extern time_t
time(time_t *timer)
{
    time_t ret = 0;

    #if defined(__linux__)
    struct timespec result = {0};
    if (__syscall2(SYS_clock_gettime, CLOCK_REALTIME,
                   (__syscall_t)&result) == 0)
    {
        ret = result.tv_sec;
    }
    #else
    #error Unimplemented target
    #endif

    if (timer != NULL)
        *timer = ret;

    return ret;
}

/* Conversion functions */

extern char *
asctime(const struct tm *timeptr)
{
    return NULL;
}

extern char *
ctime(const time_t *timer)
{
    return NULL;
}

extern struct tm *
gmtime(const time_t *timer)
{
    return NULL;
}

extern struct tm *
localtime(const time_t *timer)
{
    return NULL;
}

extern size_t
strftime(char *str, size_t maxsize, const char *format,
         const struct tm *timeptr)
{
    return 0;
}
