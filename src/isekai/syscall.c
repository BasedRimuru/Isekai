/*
This file is part of Isekai.

Isekai is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 2 of the License,
or (at your option) any later version.

Isekai is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Isekai. If not, see <https://www.gnu.org/licenses/>.
*/

#include "isekai/syscall.h"

extern __syscall_t
__syscall0(__syscall_t n)
{
    return __syscall(n, 0, 0, 0, 0, 0, 0);
}

extern __syscall_t
__syscall1(__syscall_t n, __syscall_t a)
{
    return __syscall(n, a, 0, 0, 0, 0, 0);
}

extern __syscall_t
__syscall2(__syscall_t n, __syscall_t a, __syscall_t b)
{
    return __syscall(n, a, b, 0, 0, 0, 0);
}

extern __syscall_t
__syscall3(__syscall_t n, __syscall_t a, __syscall_t b, __syscall_t c)
{
    return __syscall(n, a, b, c, 0, 0, 0);
}

extern __syscall_t
__syscall4(__syscall_t n, __syscall_t a, __syscall_t b, __syscall_t c,
                          __syscall_t d)
{
    return __syscall(n, a, b, c, d, 0, 0);
}

extern __syscall_t
__syscall5(__syscall_t n, __syscall_t a, __syscall_t b, __syscall_t c,
                          __syscall_t d, __syscall_t e)
{
    return __syscall(n, a, b, c, d, e, 0);
}

extern __syscall_t
__syscall6(__syscall_t n, __syscall_t a, __syscall_t b, __syscall_t c,
                          __syscall_t d, __syscall_t e, __syscall_t f)
{
    return __syscall(n, a, b, c, d, e, f);
}
