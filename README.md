# Isekai
A custom, C99 static libc implementation

## Project status
Currently only supports x86-64 and Linux

## Missing features
#### C89
- stdlib.h
    - malloc, calloc, realloc, free
    - strtoX functions
    - bsearch and qsort
    - multibyte char and strings
- time.h
    - mktime
    - asctime
    - ctime
    - gmtime
    - localtime
    - strftime
- stdio.h
- math.h

#### NA1
- wchar.h
- wctype.h

#### C99
- fenv.h
- inttypes.h
- complex.h
- tgmath.h
