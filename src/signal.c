/*
This file is part of Isekai.

Isekai is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 2 of the License,
or (at your option) any later version.

Isekai is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Isekai. If not, see <https://www.gnu.org/licenses/>.
*/

#include <signal.h>
#include "isekai/syscall.h"

#if defined(__linux__)
#include <sys/syscall.h>

#define SA_RESTART  0x10000000
#define SA_RESTORER 0x04000000

#else
#error Unimplemented target
#endif

static void
__restore(void)
{
}

struct k_sigaction
{
    void (*handler)(int);
    unsigned long flags;
    void (*restorer)(void);
    unsigned mask[2];
};

extern void
(*signal(int sig, void (*func)(int)))(int)
{
    void *ret = SIG_ERR;

    #if defined(__linux__)
    struct k_sigaction old = {0};
    struct k_sigaction new = {.handler = func,
                              .flags = SA_RESTART | SA_RESTORER,
                              .restorer = __restore};

    if ((signed)__syscall4(SYS_rt_sigaction, sig, (__syscall_t)&new,
                           (__syscall_t)&old, sizeof(new.mask)) >= 0)
    {
        ret = old.handler;
    }
    #else
    #error Unimplemented target
    #endif

    return ret;
}

extern int
raise(int sig)
{
    int ret = 0;

    #if defined(__linux__)
    ret = __syscall2(SYS_kill, 0, sig);
    #else
    #error Unimplemented target
    #endif

    return ret;
}
