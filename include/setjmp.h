/*
This file is part of Isekai.

Isekai is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 2 of the License,
or (at your option) any later version.

Isekai is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Isekai. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _SETJMP_H
#define _SETJMP_H

#if defined(__x86_64__) || defined(__amd64__)
typedef long jmp_buf[14];
#else
#error Unimplemented target
#endif

int setjmp(jmp_buf environment);
void longjmp(jmp_buf environment, int value);

#endif
