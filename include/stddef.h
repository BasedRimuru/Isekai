/*
This file is part of Isekai.

Isekai is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 2 of the License,
or (at your option) any later version.

Isekai is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Isekai. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _STDDEF_H
#define _STDDEF_H

#include <stdint.h>

#if defined(__x86_64__) || defined(__amd64__)
typedef uint64_t size_t;
typedef int64_t ptrdiff_t;
typedef int32_t wchar_t;
#else
#error Unimplemented target
#endif

#define NULL ((void*)0)
#define offsetof(type, member) (size_t)&(((type*)NULL)->member)

#endif
