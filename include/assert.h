/*
This file is part of Isekai.

Isekai is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 2 of the License,
or (at your option) any later version.

Isekai is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Isekai. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _ASSERT_H
#define _ASSERT_H

#ifndef NDEBUG
#define assert(x) ((void)((x) || \
                  (__assert_fail(__FILE__, __LINE__, __func__, #x), 0)))
#else
#define assert(x)((void)0)
#endif

void __assert_fail(const char *, int, const char *, const char *);

#endif
