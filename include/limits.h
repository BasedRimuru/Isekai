/*
This file is part of Isekai.

Isekai is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 2 of the License,
or (at your option) any later version.

Isekai is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Isekai. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _LIMITS_H
#define _LIMITS_H

#define CHAR_BIT 8
#define SCHAR_MIN 0x80
#define SCHAR_MAX 0x7F
#define UCHAR_MAX 0xFFU

#if ('\0' - 1) < 0
#define CHAR_MIN SCHAR_MIN
#define CHAR_MAX SCHAR_MAX
#else
#define CHAR_MIN 0x0
#define CHAR_MAX UCHAR_MAX
#endif

#define MB_LEN_MAX 16

#define SHRT_MIN  0x8000
#define SHRT_MAX  0xFFFF
#define USHRT_MAX 0xFFFFU

#define INT_MIN  0x80000000
#define INT_MAX  0x7fffffff
#define UINT_MAX 0xffffffffU

#define LONG_MIN (1UL << ((sizeof(long) * 8) - 1))
#define LONG_MAX (-(LONG_MIN + 1))
#define ULONG_MAX = ((unsigned long)-1)

#define LLONG_MIN (1ULL << ((sizeof(long long) * 8) - 1))
#define LLONG_MAX (-(LLONG_MIN + 1))
#define ULLONG_MAX = ((unsigned long long)-1)

#endif
