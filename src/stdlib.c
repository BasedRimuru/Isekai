/*
This file is part of Isekai.

Isekai is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 2 of the License,
or (at your option) any later version.

Isekai is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Isekai. If not, see <https://www.gnu.org/licenses/>.
*/

#include <ctype.h>
#include <signal.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#if defined(__linux__)
#include <sys/syscall.h>
typedef enum
{
    P_ALL = 0,
    P_PID,
    P_PGID
} idtype_t;
#define WEXITED 4
#include <sys/wait.h>
#endif

#include "isekai/syscall.h"

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

/* C runtime functions */

extern __attribute__((noreturn)) void
abort(void)
{
    for (;;)
        raise(SIGABRT);
}

static void (*__atexit[128])(void) = {NULL};
static u8 __atexit_c = 0;
extern int
atexit(void (*func)(void))
{
    int ret = 0;

    if (__atexit_c < 128)
    {
        __atexit[__atexit_c++] = func;
    }
    else
    {
        ret = -1;
    }

    return ret;
}

extern __attribute__((noreturn)) void
_Exit(int status)
{
    #if defined(__linux__)
    __syscall1(SYS_exit_group, status);
    for (;;)
        __syscall1(SYS_exit, status);
    #else
    #error Unimplemented target
    #endif
}

void _fini() __attribute__((weak));
extern __attribute__((noreturn)) void
exit(int status)
{
    while (__atexit_c != 0)
        __atexit[--(__atexit_c)]();

    if (_fini != NULL)
        _fini();

    for (;;)
        _Exit(status);
}

extern int
__libc_init(void)
{
    return 0;
}

void _init() __attribute__((weak));
int main(int argc, char *argv[], char *envp[]);
static char **__environ = {NULL};
extern int
__libc_start(int argc, char *argv[])
{
    if (_init != NULL)
        _init();

    __libc_init();

    char **envp = &(argv[argc + 1]);
    __environ = envp;

    int ret = main(argc, argv, envp);
    exit(ret);
}

/* System and environment functions */

extern int
system(const char *command)
{
    int ret = 0;

    if (command != NULL)
    {
        #if defined(__linux__)
        int pid = __syscall0(SYS_fork);
        if (pid > 0)
        {
            siginfo_t status = {0};
            if ((signed)__syscall4(SYS_waitid, P_PID, pid,
                                   (__syscall_t)&status, WEXITED) != -1)
            {
                ret = status.si_status;
            }
            else
            {
                ret = -1;
            }
        }
        else if (pid == 0)
        {
            char *argv[] = {"sh", "-c", (char*)command, NULL};
            exit(__syscall3(SYS_execve, (__syscall_t)"/bin/sh",
                            (__syscall_t)argv, (__syscall_t)__environ));
        }
        else
        {
            ret = -1;
        }
        #else
        #error Unimplemented target
        #endif
    }
    else
    {
        ret = EXIT_FAILURE;
    }

    return ret;
}

extern char *
getenv(const char *name)
{
    char *ret = NULL;

    size_t length = strlen(name);
    if (__environ[0] != NULL && name != NULL && strchr(name, '=') == NULL)
    {
        for (size_t i = 0; __environ[i] != NULL; i++)
        {
            if (strncmp(__environ[i], name, length) == 0)
            {
                ret = &(__environ[i][length + 1]);
                break;
            }
        }
    }

    return ret;
}

/* Pseudo random number generator */

static u32 __rand_seed = 1;
extern int
rand(void)
{
    u32 ret = 0;

    __rand_seed *= 1103515245;
    __rand_seed += 12345;
    ret = (__rand_seed >> 16) % 2048;

    __rand_seed *= 1103515245;
    __rand_seed += 12345;
    ret <<= 10;
    ret ^= (__rand_seed >> 16) % 1024;

    __rand_seed *= 1103515245;
    __rand_seed += 12345;
    ret <<= 10;
    ret ^= (__rand_seed >> 16) % 1024;

    return ret;
}

extern void
srand(unsigned int seed)
{
    __rand_seed = seed;
}

/* Abs and division */

extern int
abs(int x)
{
    return (x < 0) ? -x : x;
}

extern long
labs(long x)
{
    return (x < 0) ? -x: x;
}

extern long long
llabs(long long x)
{
    return (x < 0) ? -x: x;
}

extern div_t
div(int dividend, int divisor)
{
    return (div_t){.quot=(dividend / divisor),
                   .rem=(dividend % divisor)};
}

extern ldiv_t
ldiv(long dividend, long divisor)
{
    return (ldiv_t){.quot=(dividend / divisor),
                    .rem=(dividend % divisor)};
}

extern lldiv_t
lldiv(long long dividend, long long divisor)
{
    return (lldiv_t){.quot=(dividend / divisor),
                     .rem=(dividend % divisor)};
}

/* String conversion */

#define _ATOI() \
    if (str != NULL) \
    { \
        size_t i = 0; \
        while (isspace(str[i]) != 0) \
            i++; \
\
        bool inv = false; \
        if (str[i] == '+' || str[i] == '-') \
        { \
            inv = (str[i] == '-'); \
            i++; \
        } \
    \
        for (; str[i] != '\0'; i++) \
        { \
            ret *= 10; \
            if (str[i] >= '0' && str[i] <= '9') \
            { \
                ret += (str[i] - '0'); \
            } \
            else \
            { \
                break; \
            } \
        } \
    \
        if (inv) \
            ret = -ret; \
    }

extern int
atoi(const char *str)
{
    int ret = 0;

    _ATOI()

    return ret;
}

extern long
atol(const char *str)
{
    long ret = 0;

    _ATOI()

    return ret;
}

extern long long
atoll(const char *str)
{
    long long ret = 0;

    _ATOI()

    return ret;
}

extern double
atof(const char *str)
{
    double ret = 0.0;

    if (str != NULL)
    {
        size_t i = 0;
        while (isspace(str[i]) != 0)
            i++;

        bool inv = false;
        if (str[i] == '+' || str[i] == '-')
        {
            inv = (str[i] == '-');
            i++;
        }

        bool dot = false;
        double fraction = 1;
        for (; str[i] != '\0'; i++)
        {
            if (str[i] == '.')
            {
                if (!dot)
                {
                    dot = true;
                }
                else
                {
                    break;
                }
            }
            else if (str[i] >= '0' && str[i] <= '9')
            {
                if (!dot)
                {
                    ret *= 10.0;
                    ret += (double)(str[i] - '0');
                }
                else
                {
                    fraction /= 10.0;
                    ret += ((double)(str[i] - '0')) * fraction;
                }
            }
            else
            {
                break;
            }
        }

        if (inv)
            ret = -ret;

        if (str[i] == 'e' || str[i] == 'E')
        {
            /* TODO use pow when math.h becomes avaliable */
            long long exp = atoll(&(str[i + 1]));

            if (exp >= 0)
            {
                for (long long i = 0; i < exp; i++)
                    ret *= 10.0;
            }
            else
            {
                for (long long i = exp; i < 0; i++)
                    ret /= 10.0;
            }
        }
    }

    return ret;
}

extern unsigned long
strtoul(const char *str, char **endptr, int base)
{
    return 0;
}

extern unsigned long long
strtoull(const char *str, char **endptr, int base)
{
    return 0;
}

extern long
strtol(const char *str, char **endptr, int base)
{
    return 0;
}

extern long long
strtoll(const char *str, char **endptr, int base)
{
    return 0;
}

extern double
strtod(const char *str, char **endptr)
{
    return 0.0;
}

extern long double
strtold(const char *str, char **endptr)
{
    return 0.0;
}
