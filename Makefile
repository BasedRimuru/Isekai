#This file is part of Isekai.

#Isekai is free software: you can redistribute it and/or modify it
#under the terms of the GNU Lesser General Public License as published
#by the Free Software Foundation, either version 2 of the License,
#or (at your option) any later version.

#Isekai is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#See the GNU General Public License for more details.

#You should have received a copy of the GNU Lesser General Public License
#along with Isekai. If not, see <https://www.gnu.org/licenses/>.

CFLAGS += -O2 --std=gnu99 -ffreestanding -Iinclude
CFLAGS += -nolibc -nostdlib -nodefaultlibs
CFLAGS += -Wall -Wextra -Wno-unused-parameter

.PHONY: all clean

all: build/libc.a
clean:
	rm -rf build

build/libc.a: build/ctype.o build/locale.o build/signal.o \
              build/stdlib.o build/string.o build/time.o \
              build/setjmp.o \
              build/longjmp.o \
              build/_syscall.o \
              build/_syscall_n.o \
              build/crt0.o build/crti.o build/crtn.o
	ar -rcs $@ $^

build/ctype.o: src/ctype.c   | build
	gcc $(CFLAGS) -c $< -o $@
build/locale.o: src/locale.c | build
	gcc $(CFLAGS) -c $< -o $@
build/signal.o: src/signal.c | build
	gcc $(CFLAGS) -c $< -o $@
build/stdlib.o: src/stdlib.c | build
	gcc $(CFLAGS) -c $< -o $@
build/string.o: src/string.c | build
	gcc $(CFLAGS) -c $< -o $@
build/time.o: src/time.c     | build
	gcc $(CFLAGS) -c $< -o $@

build/setjmp.o: asm/x86_64/setjmp.S | build
	as $< -o $@
build/longjmp.o: asm/x86_64/longjmp.S | build
	as $< -o $@

build/_syscall.o: asm/x86_64/syscall.S | build
	as $< -o $@
build/_syscall_n.o: src/isekai/syscall.c | build
	gcc $(CFLAGS) -c $< -o $@

build/crt0.o: crt/x86_64/crt0.S | build
	as $< -o $@ 
build/crti.o: crt/x86_64/crti.S | build
	as $< -o $@ 
build/crtn.o: crt/x86_64/crtn.S | build
	as $< -o $@ 

build:
	@mkdir -p $@
