/*
This file is part of Isekai.

Isekai is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 2 of the License,
or (at your option) any later version.

Isekai is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Isekai. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef _ISEKAI_SYSCALL_H
#define _ISEKAI_SYSCALL_H

#include <stdint.h>

#if defined(__x86_64__) || defined(__amd64__)
typedef uint64_t __syscall_t;
#else
#error Unimplemented target
#endif

__syscall_t __syscall(__syscall_t n, __syscall_t a, __syscall_t b,
                      __syscall_t c, __syscall_t d, __syscall_t e,
                      __syscall_t f);

__syscall_t __syscall0(__syscall_t n);
__syscall_t __syscall1(__syscall_t n, __syscall_t a);
__syscall_t __syscall2(__syscall_t n, __syscall_t a, __syscall_t b);
__syscall_t __syscall3(__syscall_t n, __syscall_t a, __syscall_t b,
                       __syscall_t c);
__syscall_t __syscall4(__syscall_t n, __syscall_t a, __syscall_t b,
                       __syscall_t c, __syscall_t d);
__syscall_t __syscall5(__syscall_t n, __syscall_t a, __syscall_t b,
                       __syscall_t c, __syscall_t d, __syscall_t e);
__syscall_t __syscall6(__syscall_t n, __syscall_t a, __syscall_t b,
                       __syscall_t c, __syscall_t d, __syscall_t e,
                       __syscall_t f);

#endif
