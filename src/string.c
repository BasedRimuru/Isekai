/*
This file is part of Isekai.

Isekai is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 2 of the License,
or (at your option) any later version.

Isekai is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Isekai. If not, see <https://www.gnu.org/licenses/>.
*/

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

extern void *
memchr(const void *str, int c, size_t n)
{
    void *ret = NULL;

    /* Initialize magic numbers */
    uintmax_t cr = (u8)c;
    for (size_t i = 0; i < sizeof(uintmax_t); i++)
        cr = (cr << 8) + (u8)c;

    uintmax_t ones = 0x1;
    for (size_t i = 0; i < sizeof(uintmax_t); i++)
        ones = (ones << 8) + 0x1;

    uintmax_t mask = 0x80;
    for (size_t i = 0; i < sizeof(uintmax_t); i++)
        mask = (mask << 8) + 0x80;

    /* Aligns memory if necessary */
    u8 align = 0;
    u8 salign = (uintptr_t)str % sizeof(uintmax_t);
    if (n >= sizeof(uintmax_t) && salign != 0)
    {
        align = sizeof(uintmax_t) - salign;
        for (size_t i = 0; i < align; i++)
        {
            if (((u8*)str)[i] == c)
            {
                ret = &(((u8*)str)[i]);
                break;
            }
        }
        n -= align;
    }

    /* Tests if uintmax-sized blocks have one byte equal to c */
    if (ret == NULL)
    {
        uintmax_t *str_m = (uintmax_t*)str;
        for (size_t i = 0; i < (n / sizeof(uintmax_t)); i++)
        {
            /* In that case, get which byte is c */
            uintmax_t x = str_m[i] ^ cr;
            if ((bool)((x - ones) & ~(x) & mask))
            {
                for (u8 j = 0; j < sizeof(uintmax_t); j++)
                {
                    if (((u8*)&x)[j] == 0)
                    {
                        ret = &(((u8*)str)[(i * sizeof(uintmax_t) + j)]);
                        break;
                    }
                }
            }

            if (ret != NULL)
                break;
        }
    }

    /* Tests if remainder bytes have one byte equal to c */
    if (ret == NULL)
    {
        for (size_t i = (n / sizeof(uintmax_t)) * sizeof(uintmax_t);
             i < n; i++)
        {
            if (((u8*)str)[i] == c)
            {
                ret = &(((u8*)str)[i]);
                break;
            }
        }
    }

    return ret;
}

extern int
memcmp(const void *str, const void *str2, size_t n)
{
    int ret = 0;

    u8 align = 0;
    u8 salign = (uintptr_t)str % sizeof(uintmax_t);
    u8 s2align = (uintptr_t)str2 % sizeof(uintmax_t);

    /* Tries to align memory if possible */
    if (n >= sizeof(uintmax_t) && salign != 0 && salign == s2align)
    {
        align = sizeof(uintmax_t) - salign;
        for (size_t i = 0; i < align; i++)
        {
            intmax_t x = ((u8*)str)[i] - ((u8*)str2)[i];
            if (x != 0)
            {
                ret = (x > 0) ? 1 : -1;
                break;
            }
        }
        n -= align;
    }

    /* Compares uintmax-sized blocks */
    if (ret == 0)
    {
        uintmax_t *str_m = (uintmax_t*)&(((u8*)str)[align]);
        uintmax_t *str2_m = (uintmax_t*)&(((u8*)str2)[align]);
        for (size_t i = 0; i < (n / sizeof(uintmax_t)); i++)
        {
            intmax_t x = str_m[i] - str2_m[i];
            if (x != 0)
            {
                ret = (x > 0) ? 1 : -1;
                break;
            }
        }
    }

    /* Compares the remainder bytes */
    if (ret == 0)
    {
        u8 *str_8 = &(((u8*)str)[align]);
        u8 *str2_8 = &(((u8*)str2)[align]);
        for (size_t i = (n / sizeof(uintmax_t)) * sizeof(uintmax_t);
             i < n; i++)
        {
            i8 x = str_8[i] - str2_8[i];
            if (x != 0)
            {
                ret = (x > 0) ? 1 : -1;
                break;
            }
        }
    }

    return ret;
}

extern void *
memcpy(void * restrict dest, const void * restrict src, size_t n)
{
    if (dest != src && n != 0)
    {
        u8 align = 0;
        u8 dalign = (uintptr_t)dest % sizeof(uintmax_t);
        u8 salign = (uintptr_t)src % sizeof(uintmax_t);

        /* Tries to align memory if possible */
        if (n >= sizeof(uintmax_t) && dalign != 0 && dalign == salign)
        {
            align = sizeof(uintmax_t) - dalign;
            for (size_t i = 0; i < align; i++)
                ((u8*)dest)[i] = ((u8*)src)[i];
            n -= align;
        }

        /* Copies uintmax-sized blocks */
        uintmax_t *dest_m = (uintmax_t*)&(((u8*)dest)[align]);
        uintmax_t *src_m = (uintmax_t*)&(((u8*)src)[align]);
        for (size_t i = 0; i < (n / sizeof(uintmax_t)); i++)
            dest_m[i] = src_m[i];

        /* Copies the remainder bytes */
        for (size_t i = (n / sizeof(uintmax_t)) * sizeof(uintmax_t);
             i < n; i++)
        {
            (&(((u8*)dest)[align]))[i] = (&(((u8*)src)[align]))[i];
        }
    }

    return dest;
}

extern void *
memmove(void *dest, const void *src, size_t n)
{
    /* Decides the order to copy */
    if (((u8*)src + n) > (u8*)dest)
    {
        u8 align = 0;
        u8 dalign = (uintptr_t)dest % sizeof(uintmax_t);
        u8 salign = (uintptr_t)src % sizeof(uintmax_t);
        u8 *dest_a = dest;
        u8 *src_a = (u8*)src;

        /* Defines the alignment */
        if (n >= sizeof(uintmax_t) && dalign != 0 && dalign == salign)
        {
            align = sizeof(uintmax_t) - dalign;
            dest_a += align;
            src_a += align;
            n -= align;
        }

        /* Copies the remainder bytes */
        for (size_t i = n - 1;
             i >= (n / sizeof(uintmax_t)) * sizeof(uintmax_t); i--)
        {
            dest_a[i] = src_a[i];
        }

        /* Copies uintmax-sized blocks */
        size_t blocks = n / sizeof(uintmax_t);
        uintmax_t *dest_m = (uintmax_t*)dest_a;
        uintmax_t *src_m = (uintmax_t*)src_a;
        for (size_t i = 1; i <= blocks; i++)
            dest_m[blocks - i] = src_m[blocks - i];

        /* Copies the aligment bytes */
        for (size_t i = 1; i <= align; i++)
            ((u8*)dest)[align - i] = ((u8*)src)[align - i];
    }
    else
    {
        memcpy(dest, src, n);
    }

    return dest;
}

extern void *
memset(void *str, int c, size_t n)
{
    /* Initialize magic numbers */
    uintmax_t cr = (u8)c;
    for (size_t i = 0; i < sizeof(uintmax_t); i++)
        cr = (cr << 8) + (u8)c;

    /* Aligns memory if necessary */
    u8 align = 0;
    u8 salign = (uintptr_t)str % sizeof(uintmax_t);
    if (n >= sizeof(uintmax_t) && salign != 0)
    {
        align = sizeof(uintmax_t) - salign;
        for (size_t i = 0; i < align; i++)
            ((u8*)str)[i] = c;
        n -= align;
    }

    /* Sets uintmax-sized blocks */
    uintmax_t *str_m = (uintmax_t*)&(((u8*)str)[align]);
    for (size_t i = 0; i < (n / sizeof(uintmax_t)); i++)
        str_m[i] = cr;

    /* Sets the remainder bytes */
    for (size_t i = (n / sizeof(uintmax_t)) * sizeof(uintmax_t); i < n; i++)
        (&(((u8*)str)[align]))[i] = c;

    return str;
}

/* TODO optimize string functions */

extern char *
strcat(char *dest, const char *src)
{
    char *sn = memchr(src, '\0', SIZE_MAX);
    char *dn = memchr(dest, '\0', SIZE_MAX);

    memmove(dn, src, sn - src + 1);

    return dest;
}

extern char *
strncat(char *dest, const char *src, size_t n)
{
    char *dn = memchr(dest, '\0', SIZE_MAX);

    char *sn = memchr(src, '\0', n);
    if (sn != NULL)
    {
        memmove(dn, src, sn - src + 1);
    }
    else
    {
        memmove(dn, src, n);
        dn[n] = '\0';
    }

    return dest;
}

extern char *
strchr(const char *str, int c)
{
    char *ret = NULL;

    size_t sl = (char*)memchr(str, '\0', SIZE_MAX) - (char*)str;
    if (c != '\0')
    {
        ret = memchr(str, c, sl);
    }
    else
    {
        ret = (char*)&(str[sl]);
    }

    return ret;
}

extern int
strcmp(const char *str, const char *str2)
{
    size_t sl = (char*)memchr(str, '\0', SIZE_MAX) - (char*)str;
    size_t sl2 = (char*)memchr(str2, '\0', SIZE_MAX) - (char*)str2;

    size_t sm = (sl < sl2) ? sl : sl2;

    return memcmp(str, str2, sm + 1);;
}

extern int
strncmp(const char *str, const char *str2, size_t n)
{
    char *se = (char*)memchr(str, '\0', n);
    char *se2 = (char*)memchr(str2, '\0', n);
    size_t sl = (se != NULL) ? (size_t)(se - (char*)str) : (size_t)n;
    size_t sl2 = (se2 != NULL) ? (size_t)(se2 - (char*)str2) : (size_t)n;

    size_t sm = (sl < sl2) ? sl : sl2;
    sm = (sm < n) ? sm + 1 : n;

    return memcmp(str, str2, sm);
}

extern int
strcoll(const char *str, const char *str2)
{
    return strcmp(str, str2);
}

extern char *
strcpy(char *dest, const char *src)
{
    char *sn = memchr(src, '\0', SIZE_MAX);

    memcpy(dest, src, sn - src + 1);

    return dest;
}

extern char *
strncpy(char *dest, const char *src, size_t n)
{
    char *sn = memchr(src, '\0', n);
    if (sn != NULL)
    {
        memcpy(dest, src, sn - src + 1);
    }
    else
    {
        memset(dest, 0, n);
        memcpy(dest, src, n);
    }

    return dest;
}

extern size_t
strcspn(const char *str, const char *str2)
{
    size_t ret = 0;

    for (bool found = false; str[ret] != '\0' && ret < SIZE_MAX; ret++)
    {
        for (size_t j = 0; str2[j] != '\0' && j < SIZE_MAX; j++)
        {
            if (str[ret] == str2[j])
            {
                found = true;
                break;
            }
        }

        if (found)
            break;
    }

    return ret;
}

extern char *
strerror(int err)
{
    char *ret = "Unknown error";

    switch (err)
    {
        case EDOM:
            ret = "Domain error";
            break;
        case ERANGE:
            ret = "Range error";
            break;
    }

    return ret;
}

extern size_t
strlen(const char *str)
{
    return (char*)memchr(str, '\0', SIZE_MAX) - (char*)str;
}

extern char *
strpbrk(const char *str, const char *str2)
{
    char *ret = NULL;

    size_t pos = strcspn(str, str2);
    if (str[pos] != '\0')
        ret = (char*)&(str[pos]);

    return ret;
}

extern char *
strrchr(const char *str, int c)
{
    char *ret = NULL;

    if (c != '\0')
    {
        char *last = strchr(str, c);
        while (last != NULL && last[0] != '\0')
        {
            ret = last;
            last = strchr(&(ret[1]), c);
        }
    }
    else
    {
        ret = strchr(str, c);
    }

    return ret;
}

extern size_t
strspn(const char *str, const char *str2)
{
    size_t ret = 0;

    for (; str[ret] != '\0' && ret < SIZE_MAX; ret++)
    {
        bool equal = false;
        for (size_t j = 0; str2[j] != '\0' && j < SIZE_MAX; j++)
        {
            if (str[ret] == str2[j])
            {
                equal = true;
                break;
            }
        }

        if (!equal)
            break;
    }

    return ret;
}

extern char *
strstr(const char *haystack, const char *needle)
{
    char *ret = NULL;

    size_t len = strlen(needle);
    for (size_t i = 0; i < SIZE_MAX && haystack[i] != '\0'; i++)
    {
        if (strncmp(&(haystack[i]), needle, len) == 0)
            ret = (char*)&(haystack[i]);
    }

    return ret;
}

extern char *
strtok(char *str, const char *delim)
{
    static char *_str = NULL;

    if (str != NULL)
    {
        _str = str;
    }
    else
    {
        _str = &(_str[1]);
    }

    if (_str[0] != '\0')
    {
        _str = &(_str[strspn(_str, delim)]);
        str = _str;

        size_t end = strcspn(_str, delim);
        _str = &(_str[end]);
        str[end] = '\0';
    }
    else
    {
        _str = NULL;
        str = NULL;
    }

    return str;
}

extern size_t
strxfrm(char *dest, const char *src, size_t n)
{
    size_t ret = n;

    size_t len = strlen(src);
    if (len < n)
        ret = len;

    strncpy(dest, src, ret);

    return ret;
}
